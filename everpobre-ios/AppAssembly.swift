//
//  AppAssembly.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 17/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

final class AppAssembly {
    
    private(set) lazy var splitViewController = UISplitViewController()
    private(set) lazy var window = UIWindow(frame: UIScreen.main.bounds)
    private(set) lazy var noteBooksRepo: NoteBooksRepository = {
        let nb = NoteBooksRepository()
        nb.initializeNoteBooks()
        return nb
    }()
    
    private(set) lazy var masterVC = NoteBooksViewController(nbsPresenter: NoteBooksPresenter(repository: noteBooksRepo), nbPresenter: NoteBookPresenter(repository: noteBooksRepo))
    private(set) lazy var detailVC = NoteDetailsViewController(presenter: NoteDetailsPresenter())
    
    func setupAppRoot() {
        masterVC.navigationDelegate = self
        splitViewController.viewControllers = [ masterVC.wrappedInNavigation(), detailVC.wrappedInNavigation()]
        window.rootViewController = splitViewController
        window.makeKeyAndVisible()
    }
}

extension AppAssembly: NoteBooksViewControllerDelegate {
    
    func showNoteBookViewController(_ viewController: NoteBookViewController) {
        viewController.navigationDelegate = self
        splitViewController.viewControllers.first?.show(viewController, sender: nil)
    }
    
}

extension AppAssembly: NoteBookViewControllerDelegate {
    func noteBookViewController(_ viewController: NoteBookViewController, didSaveNoteBook saved: Bool) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
