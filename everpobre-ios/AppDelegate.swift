//
//  AppDelegate.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 17/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let appAssembly = AppAssembly()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        appAssembly.setupAppRoot()        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataStack.sharedInstance.saveContext()
    }
}

