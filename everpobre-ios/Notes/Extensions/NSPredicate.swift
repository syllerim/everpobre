//
//  NSPredicate.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import Foundation

extension NSPredicate {
    
    func comparisonPredicate(for attribute: String, object: Any) -> NSPredicate {
        return NSPredicate(format: "%K == %@", argumentArray: [attribute,  object])
    }
    
    func comparisonGreaterThanOrEqual(for attribute: String, object: Any) -> NSPredicate {
        return NSPredicate(format: "%K >= %@", argumentArray: [attribute,  object])
    }
    
    func comparisonLessThan(for attribute: String, object: Any) -> NSPredicate {
        return NSPredicate(format: "%K < %@", argumentArray: [attribute,  object])
    }
    
    func negativeComparisonPredicate(for attribute: String, object: Any) -> NSPredicate {
        return NSPredicate(format: "%K != %@", argumentArray: [attribute,  object])
    }
    
    func containsPredicate(for attribute: String, object: Any) -> NSPredicate {
        return NSPredicate(format: "%K CONTAINS[cd] %@", argumentArray: [attribute,  object])
    }
    
    func notNilPredicate(for attribute: String) -> NSPredicate {
        return NSPredicate(format: "%K != nil", attribute)
    }
    
    func booleanYesComparison(for attribute: String) -> NSPredicate {
        return NSPredicate(format: "%K == true", attribute)
    }
    
    func booleanNoComparison(for attribute: String) -> NSPredicate {
        return NSPredicate(format: "%K == false", attribute)
    }
    
}
