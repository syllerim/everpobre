//
//  UIViewController.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func wrappedInNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}
