//
//  NoteCD+CoreDataProperties.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData


extension NoteCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteCD> {
        return NSFetchRequest<NoteCD>(entityName: "Note")
    }

    @NSManaged public var deadline: NSDate?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var text: String?
    @NSManaged public var title: String?
    @NSManaged public var notebook: NoteBookCD?
    @NSManaged public var photos: NSSet?
    @NSManaged public var tags: NSSet?

}

// MARK: Generated accessors for photos
extension NoteCD {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: PhotoContainerCD)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: PhotoContainerCD)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}

// MARK: Generated accessors for tags
extension NoteCD {

    @objc(addTagsObject:)
    @NSManaged public func addToTags(_ value: TagCD)

    @objc(removeTagsObject:)
    @NSManaged public func removeFromTags(_ value: TagCD)

    @objc(addTags:)
    @NSManaged public func addToTags(_ values: NSSet)

    @objc(removeTags:)
    @NSManaged public func removeFromTags(_ values: NSSet)

}
