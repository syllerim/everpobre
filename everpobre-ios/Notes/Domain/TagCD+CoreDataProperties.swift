//
//  TagCD+CoreDataProperties.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData


extension TagCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TagCD> {
        return NSFetchRequest<TagCD>(entityName: "Tag")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var familyFont: String?
    @NSManaged public var fontSize: Int16
    @NSManaged public var note: NoteCD?

}
