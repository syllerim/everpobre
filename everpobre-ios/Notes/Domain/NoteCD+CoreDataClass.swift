//
//  NoteCD+CoreDataClass.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData

@objc(NoteCD)
public class NoteCD: NSManagedObject {
    
    public class func managedObject(from title: String, text txt: String, in notebook: NoteBookCD, in context: NSManagedObjectContext) -> NoteCD? {
        var entity = existingObject(with: title, in: notebook, in: context)
        
        if entity == nil {
            entity = NoteCD(context : context)
            entity?.title = title
            
            if let nb = NoteBookCD.existingObject(with: notebook.title!, in: context) {
                entity?.notebook = nb
            }
        }
        
        if let entity = entity {
            entity.text = txt
            
            //TODO Get TagCD and PhotoCD from models and set the currentNote with its values
        }
        
        return entity
    }
    
    //MARK:_ Queries
    
    class func removeAllItems(in context: NSManagedObjectContext) -> Void {
        if let items = executeFetchRequest(in: context){
            for item in items {
                context.delete(item)
            }
            
            do {
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    class func removeItem(from note: NoteCD, in notebook: NoteBookCD, in context: NSManagedObjectContext) -> Bool {
        guard let obj = existingObject(with: note.title!, in: notebook, in: context) else { return false }
        context.delete(obj)
        do {
            try context.save()
            return true
        } catch {
            return false
        }
    }
    
    public class func allNotes(from notebook: NoteBookCD, in context: NSManagedObjectContext) -> [NoteCD]? {
        let predicate = NSPredicate().comparisonPredicate(for: "notebook.title", object: notebook.title!)
        return executeFetchRequest(with: predicate, in: context)
    }
    
    public class func add(note nt:NoteCD, to noteBook: NoteBookCD, in context: NSManagedObjectContext) {
        
    }
    
    //MARK: - Helper
    
    class func existingObject(with title: String, in notebook: NoteBookCD, in context: NSManagedObjectContext) -> NoteCD? {
        let predicate1 = NSPredicate().comparisonPredicate(for: "title", object: title)
        let predicate2 = NSPredicate().comparisonPredicate(for: "notebook.title", object: notebook.title!)
        let cp = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
        let array = executeFetchRequest(with: cp, in: context)
        return array?.first
    }
    
    
    //MARK: - Internal
    
    class func executeFetchRequest(with predicate: NSPredicate? = nil, in context: NSManagedObjectContext) -> [NoteCD]?
    {
        let fetchRequest: NSFetchRequest<NoteCD> = NoteCD.fetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        return try? context.fetch(fetchRequest)
    }
}
