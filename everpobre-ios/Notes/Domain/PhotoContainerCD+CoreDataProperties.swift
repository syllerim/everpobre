//
//  PhotoContainerCD+CoreDataProperties.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData


extension PhotoContainerCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoContainerCD> {
        return NSFetchRequest<PhotoContainerCD>(entityName: "PhotoContainer")
    }

    @NSManaged public var angle: Double
    @NSManaged public var height: Double
    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var width: Double
    @NSManaged public var note: NoteCD?

}
