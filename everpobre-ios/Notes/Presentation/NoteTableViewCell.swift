//
//  NoteTableViewCell.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

public class NoteTableViewCell: UITableViewCell {
    
    let containerView = UIView()
    let noteImageView = UIImageView()
    let noteNameLabel = UILabel()
    
    var note: NoteCD?
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() -> Void {
        contentView.addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 2).isActive = true
        containerView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -2).isActive = true
        containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2).isActive = true
        containerView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 2).isActive = true
        
        containerView.addSubview(noteImageView)

        noteImageView.translatesAutoresizingMaskIntoConstraints = false
        noteImageView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        noteImageView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        noteImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        noteImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 64).isActive = true

        containerView.addSubview(noteNameLabel)
        noteNameLabel.translatesAutoresizingMaskIntoConstraints = false
        noteNameLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        noteNameLabel.leftAnchor.constraint(equalTo: noteImageView.rightAnchor, constant: 16).isActive = true
        noteNameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -16).isActive = true
        noteNameLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
    }
    
    func configureCell(with note: NoteCD) {
        self.note = note
        containerView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        noteImageView.image = UIImage.init(named: "icon-note")
        noteImageView.tintColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        noteNameLabel.attributedText = attributedString(for: note.title ?? "")
    }
    
    private func attributedString(for string: String) -> NSAttributedString {
        let attStr = NSMutableAttributedString(string: string)
        attStr.addAttributes([NSAttributedStringKey.font: UIFont(name: "Helvetica-Light", size: CGFloat(18))!,
                              NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)],
                             range: NSRange(location: 0, length: string.count))
        return attStr
    }
}
