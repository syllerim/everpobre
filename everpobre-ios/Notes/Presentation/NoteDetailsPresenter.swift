//
//  File.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import Foundation

protocol NoteDetailsView: class {
    
}

final class NoteDetailsPresenter {
    
    weak var view: NoteDetailsView?
}
