//
//  NoteDetailsViewController.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

class NoteDetailsViewController: UIViewController {
    
    // MARK: - Outlets
    
    
    // MARK: - Properties
    private let presenter: NoteDetailsPresenter
    
    // MARK: - Initialization
    init(presenter: NoteDetailsPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    // MARK: - VC LifeCycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
