//
//  NoteBookPresenter.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import Foundation

protocol NoteBookViewProtocol: class {
    func setTitle(_ title: String)
    func update(with noteBook: NoteBookCD)
    func selectNewNoteBook(from: [NoteBookCD])
    func deletedNoteBook()
}

final class NoteBookPresenter {
    private let repository: NoteBooksRepositoryProtocol
    weak var view: NoteBookViewProtocol?
    public var noteBook: NoteBookCD?
    
    init(repository: NoteBooksRepositoryProtocol) {
        self.repository = repository
    }
    
    func didLoad() {
        view?.setTitle("Notebooks")
        if let nb = self.noteBook {
            self.loadContent(for: nb)
        }
    }
    
    func delete(_ noteBook: NoteBookCD) {
        let count = noteBook.notes?.count ?? 0
        if count >= 1 {
            let data = repository.getNoteBooks()
            view?.selectNewNoteBook(from: data)
        }
        else {
            view?.deletedNoteBook()
            return repository.remove(noteBook)
        }
    }
    
    func createNoteBook(for title: String, isDefault def: Bool) -> Void {
        return repository.createNoteBook(for: title, isDefault: def)
    }
    
    func move(notes nt: [NoteCD], to notebook: NoteBookCD) {
        return repository.move(notes:nt, to: notebook)
    }
}

private extension NoteBookPresenter {
 
    func loadContent(for noteBook: NoteBookCD) {
        view?.update(with: noteBook)
    }
    
}
