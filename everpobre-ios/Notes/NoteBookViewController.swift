//
//  NoteBookViewController.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

protocol NoteBookViewControllerDelegate: class {
    func noteBookViewController(_ viewController: NoteBookViewController, didSaveNoteBook: Bool) -> Void
}

class NoteBookViewController: UIViewController {

    @IBOutlet weak var notebookTitleTexField: UITextView!
    @IBOutlet weak var isDefaultSwitch: UISwitch!
    
    // MARK: - Properties
    weak var navigationDelegate: NoteBookViewControllerDelegate?
    fileprivate let presenter: NoteBookPresenter
    var noteBook: NoteBookCD?
    var noteBooks: [NoteBookCD]?
    var selectedNoteBook: NoteBookCD?
    
    // MARK: - Initialization
    
    init(noteBook: NoteBookCD? = nil, presenter: NoteBookPresenter) {
        self.presenter = presenter
        self.presenter.noteBook  = nil
        if let nb = noteBook {
            self.noteBook = nb
            self.presenter.noteBook = nb
        }
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - VC Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigation()
        setupTextFields()

        presenter.view = self
        presenter.didLoad()
    }

    @objc func saveNoteBook() {
        presenter.createNoteBook(for: notebookTitleTexField.text!, isDefault: isDefaultSwitch.isOn)
        navigationDelegate?.noteBookViewController(self, didSaveNoteBook: true)
    }
    
    @IBAction func deleteNoteAction(_ sender: Any) {
        guard let nb = noteBook else { return }
        if nb.isDefault {
            let alertController = UIAlertController(title: "NoteBook Cannot be deleted",
                                                    message: "To delete notebook remove it as default notebook",
                                                    preferredStyle: .alert)

            let okAction = UIAlertAction(title: "OK", style: .default) { (result : UIAlertAction) in }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            if let nb = noteBook {
                presenter.delete(nb)
            }
        }
        
    }
    
    @IBAction func addNoteAction(_ sender: Any) {
        
    }
    
    
    func setupNavigation() {
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(saveNoteBook))
        navigationItem.rightBarButtonItem  = doneButton
    }
    
    func setupTextFields() {
        notebookTitleTexField.addBottomLayer()
        notebookTitleTexField.placeholder = "Notebook Name"
        notebookTitleTexField.isEditable = true
    }
}

extension NoteBookViewController: NoteBookViewProtocol {

    func setTitle(_ title: String) {
        navigationItem.title = "Notebook"
    }
    
    func update(with noteBook: NoteBookCD) {
        notebookTitleTexField.placeholder = nil
        notebookTitleTexField.text = noteBook.title
        notebookTitleTexField.isEditable = false
        isDefaultSwitch.isOn = noteBook.isDefault
    }
    
    func selectNewNoteBook(from data: [NoteBookCD]) {
        noteBooks = data
        
        let alertController = UIAlertController(title: "Select Notebook",
                                                message: "Plase select notebook to move the notes of the notebook to delete",
                                                preferredStyle: .alert)
        
        let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        alertController.view.addSubview(picker)
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.topAnchor.constraint(equalTo: alertController.view.topAnchor).isActive = true
        picker.rightAnchor.constraint(equalTo: alertController.view.rightAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: alertController.view.bottomAnchor).isActive = true
        picker.leftAnchor.constraint(equalTo: alertController.view.leftAnchor).isActive = true
        
        picker.delegate = self
        picker.dataSource = self
        picker.reloadAllComponents()
        
        let okAction = UIAlertAction(title: "Select", style: .default) { [unowned self] (result : UIAlertAction) in
            guard let nb = self.noteBook else { return }
            guard let notes = nb.notes?.sortedArray(using: [NSSortDescriptor(key: "title", ascending: true)]) as? [NoteCD] else { return }
            guard let noteBookSelected = self.selectedNoteBook else { return }
            self.presenter.move(notes: notes, to: noteBookSelected)
            self.dismiss(animated: true, completion: {
                self.navigationDelegate?.noteBookViewController(self, didSaveNoteBook: false)
            })
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deletedNoteBook() {
        cd
    }
    
}


extension NoteBookViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return noteBooks?.count ?? 0
    }
    
}

extension NoteBookViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return noteBooks?[row].title ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedNoteBook = noteBooks?[row]
    }
}
