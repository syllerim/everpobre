//
//  NoteBookCD+CoreDataProperties.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData


extension NoteBookCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteBookCD> {
        return NSFetchRequest<NoteBookCD>(entityName: "NoteBook")
    }

    @NSManaged public var title: String?
    @NSManaged public var isDefault: Bool
    @NSManaged public var notes: NSSet?

}

// MARK: Generated accessors for notes
extension NoteBookCD {

    @objc(addNotesObject:)
    @NSManaged public func addToNotes(_ value: NoteCD)

    @objc(removeNotesObject:)
    @NSManaged public func removeFromNotes(_ value: NoteCD)

    @objc(addNotes:)
    @NSManaged public func addToNotes(_ values: NSSet)

    @objc(removeNotes:)
    @NSManaged public func removeFromNotes(_ values: NSSet)

}
