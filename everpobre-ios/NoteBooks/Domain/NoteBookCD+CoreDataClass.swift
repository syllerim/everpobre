//
//  NoteBookCD+CoreDataClass.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//
//

import Foundation
import CoreData

@objc(NoteBookCD)
public class NoteBookCD: NSManagedObject {

    
    public class func managedObject(from title: String, isDefault: Bool, in context: NSManagedObjectContext) -> NoteBookCD? {
        var entity = existingObject(with: title, in: context)
        
        if entity == nil {
            entity = NoteBookCD(context : context)
            entity?.title = title
        }
        
        if let entity = entity {
            entity.isDefault = isDefault
        }
        
        return entity
    }
    
    class func existingObject(with title: String, in context: NSManagedObjectContext) -> NoteBookCD? {
        let predicate = NSPredicate().comparisonPredicate(for: "title", object: title)
        let array = executeFetchRequest(with: predicate, in: context)
        return array?.first
    }
    
    public class func add(notes nts: [NoteCD], to notebook: NoteBookCD, in context: NSManagedObjectContext) {
        if let noteBook = existingObject(with: notebook.title!, in: context) {
            noteBook.notes?.addingObjects(from: nts)
        }
    }
    
    //MARK:_ Queries
    
    public class func getAllItems(in context: NSManagedObjectContext) -> [NoteBookCD]? {
        return executeFetchRequest(in: context)
    }
    
    class func removeAllItems(in context: NSManagedObjectContext) -> Void {
        if let items = executeFetchRequest(in: context){
            for item in items {
                context.delete(item)
            }
            
            do {
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    public class func remove(notebook nb: NoteBookCD, in context: NSManagedObjectContext) -> Bool {
        guard let obj = existingObject(with: nb.title!, in: context) else { return false }
        context.delete(obj)
        do {
            try context.save()
            return true
        } catch {
            return false
        }
    }
    
    //MARK:- Internal
    
    class func executeFetchRequest(with predicate: NSPredicate? = nil, in context: NSManagedObjectContext) -> [NoteBookCD]?
    {
        let fetchRequest: NSFetchRequest<NoteBookCD> = NoteBookCD.fetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        return try? context.fetch(fetchRequest)
    }
    
    
}
