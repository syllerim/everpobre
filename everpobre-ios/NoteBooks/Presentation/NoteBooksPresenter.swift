//
//  NoteBooksPresenter.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import Foundation
import CoreData

protocol NoteBooksView: class {
    func setTitle(_ title: String)
    func update(with noteBooks: [NoteBookCD])
}

final class NoteBooksPresenter {
    private let repository: NoteBooksRepositoryProtocol
    weak var view: NoteBooksView?
    
    init(repository: NoteBooksRepositoryProtocol) {
        self.repository = repository
    }
    
    func didLoad() {
        view?.setTitle("Notebooks")
        NotificationCenter.default.addObserver(self, selector: #selector(contextObjectsDidChange(_:)), name: Notification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
        loadContent()
    }
    
    @objc func contextObjectsDidChange(_ notification: Notification) {
        loadContent()
    }
}

private extension NoteBooksPresenter {
    func loadContent() {
        let noteBooks = repository.getNoteBooks()
        self.view?.update(with : noteBooks)
    }
}
