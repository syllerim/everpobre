//
//  NoteBooksViewController+TableView.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

extension NoteBooksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noteBooks[section].notes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "NoteBookTableViewCell")!
    }
}

extension NoteBooksViewController: UITableViewDelegate {
    
    // MARK: - Sections
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return noteBooks.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return noteBooks[section].title
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let noteBookView = NoteBookView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 45))
        noteBookView.configureCell(with: noteBooks[section])
        noteBookView.delegate = self
        return noteBookView
    }
    
    // MARK: - Cells
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let nb = noteBooks[indexPath.section]
        if let notes = nb.notes?.sortedArray(using: [NSSortDescriptor(key: "title", ascending: true)]) as? [NoteCD] {
            (cell as? NoteTableViewCell)?.configureCell(with: notes[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
}
