//
//  NoteBookTableViewCell.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 22/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

protocol NoteBookViewDelegate: class {
    func noteBookView(_ view: UIView, didSelected: NoteBookCD)
}

final class NoteBookView: UIView {
    
    let containerView = UIView()
    let notebookImageView = UIImageView()
    let notebookNameLabel = UILabel()
    let notebookButton = UIButton()
    
    var noteBook: NoteBookCD?
    
    weak var delegate: NoteBookViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() -> Void {
        self.addSubview(containerView)

        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true

        containerView.addSubview(notebookImageView)
        notebookImageView.translatesAutoresizingMaskIntoConstraints = false
        notebookImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        notebookImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 16).isActive = true
        notebookImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        notebookImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true

        containerView.addSubview(notebookNameLabel)
        notebookNameLabel.translatesAutoresizingMaskIntoConstraints = false
        notebookNameLabel.heightAnchor.constraint(equalToConstant: 21).isActive = true
        notebookNameLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        notebookNameLabel.leftAnchor.constraint(equalTo: notebookImageView.rightAnchor, constant: 16).isActive = true
        
        containerView.addSubview(notebookButton)
        notebookButton.translatesAutoresizingMaskIntoConstraints = false
        notebookButton.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -16).isActive = true
        notebookButton.widthAnchor.constraint(equalToConstant: 16).isActive = true
        notebookButton.heightAnchor.constraint(equalToConstant: 16).isActive = true
        notebookButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true

        notebookNameLabel.rightAnchor.constraint(lessThanOrEqualTo: notebookButton.leftAnchor, constant: -16).isActive = true
    }
    
    func configureCell(with noteBook: NoteBookCD) {
        self.noteBook = noteBook
        containerView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        notebookImageView.image = UIImage.init(named: "icon-notebook")
        notebookImageView.tintColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
        notebookNameLabel.attributedText = attributedString(for: noteBook.title ?? "")
        notebookButton.setImage(#imageLiteral(resourceName: "icon-details"), for: .normal)
        notebookButton.addTarget(self, action: #selector(btnTouched), for: .touchUpInside)
    }
    
    private func attributedString(for string: String) -> NSAttributedString {
        let attStr = NSMutableAttributedString(string: string)
        attStr.addAttributes([NSAttributedStringKey.font: UIFont(name: "Helvetica-Light", size: CGFloat(18))!,
                              NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)],
                             range: NSRange(location: 0, length: string.count))
        return attStr
    }
    
    @objc public func btnTouched() {
        guard let noteBook = noteBook else { return }
        delegate?.noteBookView(self, didSelected: noteBook)
    }
}
