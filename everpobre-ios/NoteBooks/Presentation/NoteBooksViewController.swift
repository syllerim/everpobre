//
//  NoteBooksViewController.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import UIKit

protocol NoteBooksViewControllerDelegate: class {
    func showNoteBookViewController(_ viewController: NoteBookViewController) -> Void
}

final class NoteBooksViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var noteBooksLabel: UILabel!
    @IBOutlet weak var addNoteBookButton: UIButton!
    @IBOutlet weak var noteBooksTableView: UITableView!
    
    // MARK: - Properties
    private let nbsPresenter: NoteBooksPresenter
    private let nbPresenter: NoteBookPresenter
    var noteBooks = [NoteBookCD]()
    weak var navigationDelegate: NoteBooksViewControllerDelegate?
    
    // MARK: - Initialization
    
    init(nbsPresenter: NoteBooksPresenter, nbPresenter: NoteBookPresenter) {
        self.nbsPresenter = nbsPresenter
        self.nbPresenter = nbPresenter
        
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    // MARK: - VC LifeCycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        nbsPresenter.view = self
        nbsPresenter.didLoad()
    }
    
    // MARK: - Actions
    
    @IBAction func addNoteBook(_ sender: Any) {
        let notebookVC = NoteBookViewController(presenter: nbPresenter)
        self.navigationDelegate?.showNoteBookViewController(notebookVC)
    }
}

extension NoteBooksViewController: NoteBooksView {
    
    func setTitle(_ title: String) {
        noteBooksLabel.text = title
    }
    
    func update(with noteBooks: [NoteBookCD]) {
        self.noteBooks = noteBooks
        noteBooksTableView.reloadData()
    }
    
}

extension NoteBooksViewController {
    func setupTableView() {
        noteBooksTableView.delegate = self
        noteBooksTableView.dataSource = self
        noteBooksTableView.allowsSelection = true
        noteBooksTableView.separatorStyle = .singleLine
        noteBooksTableView.register(NoteTableViewCell.self, forCellReuseIdentifier: "NoteBookTableViewCell")
    }
}

extension NoteBooksViewController: NoteBookViewDelegate {
    
    func noteBookView(_ view: UIView, didSelected noteBook: NoteBookCD) {
        let notebookVC = NoteBookViewController(noteBook: noteBook, presenter: nbPresenter)
        self.navigationDelegate?.showNoteBookViewController(notebookVC)
    }
    
}
