//
//  NoteBooksRepository.swift
//  everpobre-ios
//
//  Created by Mirellys Arteta Davila on 21/04/2018.
//  Copyright © 2018 syllerim. All rights reserved.
//

import Foundation
import CoreData

protocol NoteBooksRepositoryProtocol {
    func initializeNoteBooks()
    func getNoteBooks() -> [NoteBookCD]
    func createNoteBook(for title: String, isDefault: Bool) -> Void
    func move(notes nts: [NoteCD], to notebook: NoteBookCD)
    func remove(_ noteBook: NoteBookCD) -> Void
}

final class NoteBooksRepository: NoteBooksRepositoryProtocol {
    
    func initializeNoteBooks() -> Void {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        
        context.performAndWait {
            
            if let noteBook = NoteBookCD.managedObject(from: "- Default Notebook", isDefault: true, in: context) {
                _ = NoteCD.managedObject(from: "Note Everpobre", text: "", in: noteBook, in: context)
                
                //Persist
                do {
                    try context.save()
                } catch {
                    fatalError("Failure to save context: \(error)")
                }
            }
        }
    }
    
    func getNoteBooks() -> [NoteBookCD] {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        guard let noteBooks = NoteBookCD.getAllItems(in: context) else { return [NoteBookCD]() }
        return noteBooks
    }
    
    func createNoteBook(for title: String, isDefault: Bool) -> Void {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        
        context.performAndWait {
            
            _ = NoteBookCD.managedObject(from: title, isDefault: isDefault, in: context)
            
            do {
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
        }
    }
    
    func remove(_ noteBook: NoteBookCD) {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        context.performAndWait {
            _ = NoteBookCD.remove(notebook: noteBook, in: context)
        }
        
    }
    
    func move(notes nts: [NoteCD], to notebook: NoteBookCD) {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        context.performAndWait {
            NoteBookCD.add(notes: nts, to: notebook, in: context)
            _ = NoteBookCD.remove(notebook: notebook, in: context)
           
            do {
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
        }
    }
}
